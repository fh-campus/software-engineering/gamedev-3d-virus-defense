﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Target : MonoBehaviour
{
    public float health = 5.0f;
    public int pointValue;

    public ParticleSystem DestroyedEffect;

    [Header("Audio")]
    public RandomPlayer HitPlayer;
    public AudioSource IdleSource;
    
    public bool Destroyed => m_Destroyed;

    bool m_Destroyed = false;
    float m_CurrentHealth;

    //[SerializeField]
    float Speed = 5f;
    [SerializeField]
    float StopDistance = 1.5f;
    float RayCastDistance = 100f;

    void Awake()
    {
        Helpers.RecursiveLayerChange(transform, LayerMask.NameToLayer("Target"));
    }

    void Start()
    {
        if(DestroyedEffect)
            PoolSystem.Instance.InitPool(DestroyedEffect, 16);
        
        m_CurrentHealth = health;
        if(IdleSource != null)
            IdleSource.time = Random.Range(0.0f, IdleSource.clip.length);
    }

    public void Update()
    {
        Transform target = GameObject.FindWithTag("Player").transform;
        transform.LookAt(target.position);
        //transform.position += Time.deltaTime  
        //if (Physics.Raycast(transform.position, transform.forward, RayCastDistance))
        //Ray ray = new Ray(transform.position, transform.forward);

        //Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        //Debug.DrawRay(transform.position, forward, Color.green);
        
        //if (Physics.Raycast(ray, ))
        //RaycastHit[] hits = Physics.RaycastAll(ray);

        // Raycast Ergebnisse nach Distanz sortieren
        //Array.Sort(hits, (RaycastHit x, RaycastHit y) => x.distance.CompareTo(y.distance));

        // Sichtbar und nicht zu nah? => näher kommen
        if (//IsTargetVisible(Camera.main, gameObject) &&
            Vector3.Distance(transform.position, target.position) > StopDistance)
            transform.Translate(0, 0, Speed * Time.deltaTime);
        
    }

    //bool IsTargetVisible(Camera c, GameObject go)
    //{
    //    var planes = GeometryUtility.CalculateFrustumPlanes(c);
    //    var point = go.transform.position;
    //    foreach (var plane in planes)
    //    {
    //        if (plane.GetDistanceToPoint(point) < 0)
    //            return false;
    //    }
    //    return true;
    //}



    public void Got(float damage)
    {
        m_CurrentHealth -= damage;
        
        if(HitPlayer != null)
            HitPlayer.PlayRandom();
        
        if(m_CurrentHealth > 0)
            return;

        Vector3 position = transform.position;
        
        //the audiosource of the target will get destroyed, so we need to grab a world one and play the clip through it
        if (HitPlayer != null)
        {
            var source = WorldAudioPool.GetWorldSFXSource();
            source.transform.position = position;
            source.pitch = HitPlayer.source.pitch;
            source.PlayOneShot(HitPlayer.GetRandomClip());
        }

        if (DestroyedEffect != null)
        {
            var effect = PoolSystem.Instance.GetInstance<ParticleSystem>(DestroyedEffect);
            effect.time = 0.0f;
            effect.Play();
            effect.transform.position = position;
        }

        m_Destroyed = true;
        
        gameObject.SetActive(false);
       
        GameSystem.Instance.TargetDestroyed(pointValue);
    }
}
